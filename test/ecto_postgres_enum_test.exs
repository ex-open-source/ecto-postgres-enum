defmodule MyEnum do
  use EctoPostgresEnum, values: [:first, :second]
end

defmodule EctoPostgresEnumTest do
  use ExUnit.Case

  defmodule MySchema do
    use Ecto.Schema

    alias Ecto.Changeset

    embedded_schema do
      field(:my_field, MyEnum)
    end

    def changeset(params), do: Changeset.cast(%__MODULE__{}, params, [:my_field])
  end

  require MyEnum

  test "changeset" do
    assert MySchema.changeset(%{"my_field" => :first}).valid? == true
    assert MySchema.changeset(%{"my_field" => "first"}).valid? == true
    assert MySchema.changeset(%{"my_field" => nil}).valid? == true
    assert MySchema.changeset(%{"my_field" => :third}).valid? == false
    assert MySchema.changeset(%{"my_field" => 0}).valid? == false
  end

  test "debug" do
    assert MyEnum.atom_values() == [:first, :second]
    assert MyEnum.string_values() == ["first", "second"]
    assert MyEnum.valid_atom?(:first) == true
    assert MyEnum.valid_atom?("first") == false
    assert MyEnum.valid_atom?(:third) == false
    assert MyEnum.valid_atom?(nil) == false
    assert MyEnum.valid_atom?(0) == false
    assert MyEnum.valid_string?("first") == true
    assert MyEnum.valid_string?(:first) == false
    assert MyEnum.valid_string?("third") == false
    assert MyEnum.valid_string?(nil) == false
    assert MyEnum.valid_string?(0) == false
    assert MyEnum.valid?(:first) == true
    assert MyEnum.valid?("first") == true
    assert MyEnum.valid?(:third) == false
    assert MyEnum.valid?(nil) == false
    assert MyEnum.valid?(0) == false
    assert MyEnum.values() == [first: "first", second: "second"]
  end
end
