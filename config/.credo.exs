%{
  configs: [
    %{
      checks: [
        {Credo.Check.Refactor.MapInto, false},
        {Credo.Check.Warning.LazyLogging, false}
      ],
      files: %{included: ["lib/"], excluded: []},
      name: "default"
    }
  ]
}
