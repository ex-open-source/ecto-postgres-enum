defmodule EctoPostgresEnum.Mixfile do
  use Mix.Project

  @version "1.1.0"

  def application, do: [applications: [:logger, :ecto]]

  def project do
    [
      app: :ecto_postgres_enum,
      deps: [
        {:credo, "~> 1.1.0", only: [:test], runtime: false},
        {:dialyxir, "~> 1.0.0-rc.4", only: [:dialyzer], runtime: false},
        {:ecto_sql, "~> 3.0"},
        {:ex_doc, "~> 0.21", only: :docs},
        {:excoveralls, "~> 0.10", only: :test}
      ],
      description: "Ecto helper library to use PostgreSQL enums",
      dialyzer: [ignore_warnings: "config/dialyzer.ignore-warnings"],
      docs: [
        extras: ["README.md": [filename: "ecto_postgres_enum"]],
        main: "ecto_postgres_enum",
        source_ref: "v#{@version}",
        source_url: "https://gitlab.com/ex-open-source/ecto-postgres-enum"
      ],
      elixir: "~> 1.6",
      elixirc_paths: ["lib" | elixirc_paths(Mix.env())],
      name: "EctoPostgresEnum",
      package: [
        files: ~w(README.md lib mix.exs),
        licenses: ["MIT"],
        links: %{gitlab: "https://gitlab.com/ex-open-source/ecto-postgres-enum"},
        maintainers: ["Tomasz Sulima"]
      ],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        credo: :test,
        dialyzer: :dialyzer,
        "dialyzer.build": :dialyzer,
        "dialyzer.clean": :dialyzer,
        "dialyzer.explain": :dialyzer,
        docs: :docs
      ],
      test_coverage: [tool: ExCoveralls],
      version: @version
    ]
  end

  defp elixirc_paths(:test), do: ["test/support"]
  defp elixirc_paths(_env), do: []
end
